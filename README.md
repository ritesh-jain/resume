# Résumé
> LuaHBTeX - Version 1.12.0
> 
> TeX Live - Version 2020/Debian

This repository holds the source-code for my personal résumé. 
It's written in LaTeX and uses LuaLaTeX for generating dynamic components and injecting data from external json file.

## Installing / Getting started

To get this project up & running you need to first install LaTeX for your system. You can follow the link below for help.

Remember to install the Full version to get the LuaLaTeX compiler.

> https://www.latex-project.org/get/

To verify the installation you can execute the code below.
```shell
lualatex --version
```

You should see the version of LuaLaTeX like this.
```shell
This is LuaHBTeX, Version 1.12.0 (TeX Live 2020/Debian)
```

If you get error then either LaTeX is not installed properly, or it's not the Full version, or it's not added to $PATH properly.

### Initial Configuration

`/src/data.json` file contains all the bio-data. You need to modify it and replace it with your own data.

## Developing

The Styling of the document can be configured in `/src/styles.tex` file.

`/src/components.cls` file provides reusable components and related helper functions. It also handles all the boilerplate code and dependency imports to keep the main file clean.

### Building

To build the document, run the following commands:

```shell
echo "Cleaning up old files..."
rm -rf ./out
mkdir out
echo "Building..."
lualatex -output-directory=./out/ ./src/main.tex
mkdir out/dist
mv out/main.pdf out/dist/resume.pdf
```

The generated document will be located at `/out/dist/resume.pdf`.

### Deploying

This project implements CI/CD using [Gitlab Pipelines](https://docs.gitlab.com/ee/ci/pipelines/).

All the relevant code can be found in `.gitlab-ci.yml` file in project root.
Currently, it builds the projects and uses AWS CLI with docker to upload the generated PDF file to my S3 bucket [Here](https://myresourcefiles.s3.ap-south-1.amazonaws.com/resume/resume.pdf).

## Links

- Project homepage: https://www.riteshj.com/resume
- Repository: https://gitlab.com/ritesh-jain/resume
- Issue tracker: https://gitlab.com/ritesh-jain/resume/-/issues
- Related projects:
    - My personal website: https://www.riteshj.com


## Licensing
The code in this project is licensed under MIT license.
