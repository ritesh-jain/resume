% This file provides reusable components and related helper functions.
% It also handles all the boilerplate code and dependency imports to keep the main file clean.



% ----- Boilerplate Code ----- %

\NeedsTeXFormat{LaTeX2e}[2020-10-01]
\ProvidesClass{components}[2021/05/17 Components, Class for my custom components.]
\ProcessOptions\relax



% ----- Imports and Dependencies ----- %

\RequirePackage{luacode}
\LoadClass{article}
\RequirePackage[margin=2cm]{geometry}
\RequirePackage[fixed]{fontawesome5}
\RequirePackage[newcommands]{ragged2e}
\RequirePackage{hyperref}
\hypersetup{hidelinks}
\RequirePackage{xcolor}
\RequirePackage{tikz}
\RequirePackage[skins]{tcolorbox}
\RequirePackage{paracol}
\RequirePackage{dashrule}
\RequirePackage{multirow,tabularx}
\RequirePackage[inline]{enumitem}
\setlist{leftmargin=*,labelsep=0.5em,nosep,itemsep=0.25\baselineskip,after=\vspace{0.25\baselineskip}}
\RequirePackage{datetime2}
\RequirePackage{fancyhdr}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}



% ----- Components ----- %

% ----- Footer ----- %
\DTMsavenow{currentDate}
\lfoot{Last updated on {\DTMfetchday{currentDate}} {\DTMenglishmonthname{\DTMfetchmonth{currentDate}}}, {\DTMfetchyear{currentDate}}}
\cfoot{}
\rfoot{\textcolor{primaryColor}{\href{https://gitlab.com/ritesh-jain/resume/}{Made with \LaTeX}}}

% Common Components

\newcommand{\sectionTitle}[2][]{
    \bigskip
    \bigskip
    \ifstrequal{#1}{}{}{\marginpar{\vspace*{\dimexpr1pt-\baselineskip}\raggedright\input{#1}}}
    {\color{primaryColorDark}\sectionHeaderStyle\MakeUppercase{#2}}\\[-1ex]
    {\color{secondaryColor}\rule{\linewidth}{2pt}\par}\medskip
}

\newcommand{\listItemTitle}[1]{
    {\large\color{primaryColor}#1}
}

\newcommand{\listItemSubtitle}[1]{
    \large{\color{textColor}|  #1}
}

\newcommand{\listItemDate}[1]{
    \hfill\small\makebox[0px][r]{\color{primaryColor}\faCalendar\color{textColor}~#1}\\
}

\newcommand{\tagButton}[1]{
    \vspace{0.15ex}
    \tikz[baseline]\node[anchor=base,draw=secondaryColor,rounded corners,inner xsep=1ex,inner ysep=0.75ex,text height=1.5ex,text depth=.25ex]{\textcolor{primaryColor}{#1}};
}

\newcommand{\divider}{
    \textcolor{secondaryColor}{\hdashrule{\linewidth}{0.6pt}{0.5ex}}\medskip
}

\newcommand{\createTagsSection}[2]{
    \sectionTitle{#1}

    \directlua{
        for key,value in ipairs(#2) do
            tex.print("\\tagButton{"..value.."}")
        end
    }
}

% Header Components.

\newcommand{\makeHeaderInfoTag}[2]{
    \mbox{\color{primaryColor}~#1\color{textColor}#2}
}

\newcommand{\makeHeaderLinkTag}[3]{
    \mbox{\color{primaryColor}~#1\href{#3#2}{\color{textColor}#2}}
}

% Education Components.

\newcommand{\educationGrade}[1]{
    \small\makebox[0.5\linewidth][l]{\color{primaryColor}\faGraduationCap\color{textColor}~#1}\\
}

% Experience Components.

\newcommand{\experienceInfo}[1]{
    \small\makebox[0px][l]{\color{primaryColor}\faBriefcase\color{textColor}~#1}
}

\newcommand{\experiencePlace}[1]{
    \hfill\small\makebox[0px][r]{\color{primaryColor}\faMapMarker\color{textColor}~#1}
}

% Achievements Components.

\newcommand{\achievementTitle}[1]{
    \begin{tabularx}{1.2\linewidth}{@{}m{2em} @{\hspace{1ex}} >{\raggedright\arraybackslash}X@{}}
        \multirow{2}{*}{\Large\color{primaryColor}{\faTrophy}} & \multirow{2}{\linewidth}{\bfseries\textcolor{primaryColor}{#1}}\\
    \end{tabularx}
    \smallskip
}

\newcommand{\achievementInfo}[1]{
    \begin{minipage}{1.2\linewidth}
        \normalsize\color{textColor}#1
    \end{minipage}
    \medskip
}

% Project Components.

\newcommand{\projectStack}[1]{
    \small\makebox[0.5\linewidth][l]{\color{primaryColor}\faInfoCircle\color{primaryColor}\textbf{\textit{~#1}}}
}
